package net.xtr3m3k.mccli.netty;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.xtr3m3k.mccli.PlayerConnection;
import net.xtr3m3k.mccli.protocol.Packet;

public class PacketHandler extends SimpleChannelInboundHandler<Packet> {
    private PlayerConnection connection;

    public PacketHandler(PlayerConnection connection) {
        this.connection = connection;
    }

    @Override protected void messageReceived(ChannelHandlerContext channelHandlerContext, Packet packet) throws Exception {
        packet.handle(connection);
    }
}

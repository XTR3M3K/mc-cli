package net.xtr3m3k.mccli.netty;


import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.*;
import net.xtr3m3k.mccli.PlayerConnection;

public class ClientInitializer extends ChannelInitializer<Channel> {
    public static final String PACKET_CODER = "packet-coder";
    public static final String PACKET_HANDLER = "packet-handler";
    public static final String FRAME_PREPENDER = "frame-prepender";

    private Channel channel;
    private ChannelPipeline pipeline;
    private PlayerConnection connection;

    public ClientInitializer(PlayerConnection connection) {
        this.connection = connection;
    }

    @Override
    public void initChannel(Channel channel) throws Exception {
        this.channel = channel;
        this.connection.setChannel(channel);
        this.pipeline = channel.pipeline();
        this.channel.config().setOption(ChannelOption.IP_TOS, 0x18);
        this.channel.config().setAllocator(PooledByteBufAllocator.DEFAULT);
        this.pipeline.addLast(FRAME_PREPENDER, new FramePrepender());
        this.pipeline.addLast(PACKET_CODER, new MinecraftCoder());
        this.pipeline.addLast(PACKET_HANDLER, new PacketHandler(connection));
    }

    @Override public void close(ChannelHandlerContext ctx, ChannelPromise promise) throws Exception {
        ctx.close();
    }

    @Override public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }
}

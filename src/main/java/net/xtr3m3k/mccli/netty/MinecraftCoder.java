package net.xtr3m3k.mccli.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageCodec;
import net.xtr3m3k.mccli.protocol.Packet;
import net.xtr3m3k.mccli.protocol.Protocol;

import java.util.List;

public class MinecraftCoder extends MessageToMessageCodec<ByteBuf, Packet> {
    @Override protected void encode(ChannelHandlerContext ctx, Packet packet, List<Object> list) throws Exception {
        ByteBuf out = ctx.alloc().buffer();
        Packet.writeVarInt(packet.getId(), out);
        packet.write(out);
        list.add(out);
        System.out.println("polecial");
    }

    @Override protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> list) throws Exception {
        int packetId = in.readInt();
        Packet packet = Protocol.createPacket(packetId);
        if (packet != null) {
            packet.read(in);
            list.add(packet);
            if (in.readableBytes() > 0) {
                throw new Exception("Did not read all bytes from packet " + packet.getClass() + " " + packetId);
            }
        }
    }
}

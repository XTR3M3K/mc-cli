package net.xtr3m3k.mccli.netty;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.EmptyByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;
import net.xtr3m3k.mccli.protocol.Packet;

import java.util.List;

public class FramePrepender extends ByteToMessageCodec<ByteBuf> {
    private static int varintSize(int paramInt) {
        if ((paramInt & 0xFFFFFF80) == 0) {
            return 1;
        }
        if ((paramInt & 0xFFFFC000) == 0) {
            return 2;
        }
        if ((paramInt & 0xFFE00000) == 0) {
            return 3;
        }
        if ((paramInt & 0xF0000000) == 0) {
            return 4;
        }
        return 5;
    }

    @Override protected void encode(ChannelHandlerContext ctx, ByteBuf msg, ByteBuf out) {
        try {
            int bodyLen = msg.readableBytes();
            int headerLen = varintSize(bodyLen);

            out.ensureWritable(headerLen + bodyLen);

            Packet.writeVarInt(bodyLen, out);
            out.writeBytes(msg);
        } catch (Exception e) {
            System.out.println("Wystapil problem z nadaniem headera powod:" + e.getMessage());
        }
    }

    @Override protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
        System.out.println("inbound");
        try {
            if (in instanceof EmptyByteBuf || in.readableBytes() < 4) return;
            in.markReaderIndex();
            int length = Packet.readVarInt(in);
            if (length > in.readableBytes()) {
                in.resetReaderIndex();
                return;
            }
            ByteBuf packet = ctx.alloc().buffer(length);
            in.readBytes(packet, length);
            out.add(packet);
        } catch (Exception e) {
            System.out.println("Wystapil problem z przeczytaniem headera powod:" + e.getMessage());
        }
    }
}
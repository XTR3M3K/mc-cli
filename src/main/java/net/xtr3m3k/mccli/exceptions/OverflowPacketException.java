package net.xtr3m3k.mccli.exceptions;

public class OverflowPacketException extends RuntimeException {
    public OverflowPacketException(String message) {
        super(message);
    }
}

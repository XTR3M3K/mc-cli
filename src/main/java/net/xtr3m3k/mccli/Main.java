package net.xtr3m3k.mccli;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {
    private static ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public static void main(String[] args) throws InterruptedException {
        PlayerConnection connection = new PlayerConnection("temporaryUser");
        connection.bind("localhost", 25577);
        executorService.scheduleWithFixedDelay(() -> {
            System.out.println("I'm alive!");
        }, 20L, 10L, TimeUnit.SECONDS);
    }
}

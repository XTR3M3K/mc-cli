package net.xtr3m3k.mccli;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import net.xtr3m3k.mccli.netty.ClientInitializer;
import net.xtr3m3k.mccli.packets.Handshake;
import net.xtr3m3k.mccli.packets.LoginRequest;
import net.xtr3m3k.mccli.packets.LoginSuccess;
import net.xtr3m3k.mccli.protocol.AbstractPacketHandler;
import net.xtr3m3k.mccli.protocol.Packet;

public class PlayerConnection extends AbstractPacketHandler {
    private NioEventLoopGroup accept;
    private Bootstrap bootstrap;
    private String name;
    private Channel channel;
    private ChannelFuture future;

    public PlayerConnection(String name) {
        this.name = name;
        this.bootstrap = new Bootstrap();
        this.accept = new NioEventLoopGroup(1);
        this.bootstrap.group(this.accept);
        this.bootstrap.handler(new ClientInitializer(this));
        this.bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
        this.bootstrap.channel(NioSocketChannel.class);
    }

    public void bind(String host, int port) {
        this.future = this.bootstrap.connect(host, port).syncUninterruptibly();
        run();
        initProtocol(host, port);
    }

    private void initProtocol(String host, int port) {
        sendPacket(new Handshake(47, host, port, 2));
        sendPacket(new LoginRequest(this.name));
    }

    private void run() {
        if (future.isSuccess()) {
            System.out.println("Boostrap zbindowany na: " + future.channel().localAddress());
            this.channel = future.channel();
        } else {
            System.out.println("Wystapil problem z bindowaniem netty powod: " + future.cause().getMessage());
        }
    }

    @Override public void handle(LoginSuccess loginSuccess) {
        System.out.println("Zalogowano pomyslnie!");
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public void sendPacket(Packet packet) {
        this.channel.writeAndFlush(packet).addListener(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE);
    }
}

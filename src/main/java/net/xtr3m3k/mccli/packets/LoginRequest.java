/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.xtr3m3k.mccli.packets;

import io.netty.buffer.ByteBuf;
import net.xtr3m3k.mccli.protocol.AbstractPacketHandler;
import net.xtr3m3k.mccli.protocol.Packet;

public class LoginRequest extends Packet {
    private String nickname;

    public LoginRequest() {
        super(0x00);
    }

    public LoginRequest(String nickname) {
        super(0x00);
        this.nickname = nickname;
    }

    @Override
    public void read(ByteBuf buf) {
        nickname = readString(buf);
    }

    @Override
    public void write(ByteBuf buf) {
        writeString(nickname, buf);
    }

    @Override
    public void handle(AbstractPacketHandler handler) throws Exception {
        //handler.handle(this);
    }
}

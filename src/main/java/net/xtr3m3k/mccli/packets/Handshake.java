package net.xtr3m3k.mccli.packets;

import io.netty.buffer.ByteBuf;
import net.xtr3m3k.mccli.protocol.AbstractPacketHandler;
import net.xtr3m3k.mccli.protocol.Packet;

public class Handshake extends Packet {
    private int protocolVersion;
    private String host;
    private int port;
    private int requestedProtocol;

    public Handshake() {
        super(0x00);
    }

    public Handshake(int protocolVersion, String host, int port, int requestedProtocol) {
        super(0x00);
        this.protocolVersion = protocolVersion;
        this.host = host;
        this.port = port;
        this.requestedProtocol = requestedProtocol;
    }

    @Override
    public void read(ByteBuf buf) {
        protocolVersion = readVarInt(buf);
        host = readString(buf);
        port = buf.readUnsignedShort();
        requestedProtocol = readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf) {
        writeVarInt(protocolVersion, buf);
        writeString(host, buf);
        buf.writeShort(port);
        writeVarInt(requestedProtocol, buf);
    }

    @Override
    public void handle(AbstractPacketHandler handler) throws Exception {
        //handler.handle(this);
    }
}

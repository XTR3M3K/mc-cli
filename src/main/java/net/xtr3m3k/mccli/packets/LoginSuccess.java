package net.xtr3m3k.mccli.packets;

import io.netty.buffer.ByteBuf;
import net.xtr3m3k.mccli.protocol.AbstractPacketHandler;
import net.xtr3m3k.mccli.protocol.Packet;

public class LoginSuccess extends Packet {
    private String uuid;
    private String username;

    public LoginSuccess() {
        super(0x02);
    }

    @Override
    public void read(ByteBuf buf) {
        uuid = readString(buf);
        username = readString(buf);
    }

    @Override
    public void write(ByteBuf buf) {
        writeString(uuid, buf);
        writeString(username, buf);
    }

    @Override
    public void handle(AbstractPacketHandler handler) throws Exception {
        handler.handle(this);
    }
}

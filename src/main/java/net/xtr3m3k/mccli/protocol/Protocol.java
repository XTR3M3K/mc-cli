package net.xtr3m3k.mccli.protocol;

import net.xtr3m3k.mccli.packets.LoginSuccess;

import java.util.HashMap;

public class Protocol {
    private static final HashMap<Integer, Class<? extends Packet>> BY_ID = new HashMap<>();
    private static final HashMap<Class<? extends Packet>, Integer> BY_CLASS = new HashMap<>();

    static {
        register(0x02, LoginSuccess.class);
    }

    public static void register(int id, Class<? extends Packet> packetClass) {
        BY_ID.put(id, packetClass);
        BY_CLASS.put(packetClass, id);
    }

    public static Packet createPacket(int id) {
        try {
            return BY_ID.get(id).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
